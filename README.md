# Gitlab Enterprise Edition Docker Compose

<div>
 <a href="https://about.gitlab.com/">
   <img src="https://img.shields.io/badge/Gitlab-latest-blue">
 </a>
 <a href="https://about.gitlab.com/">
   <img src="https://img.shields.io/badge/Gitlab%20Runner-latest-blue">
 </a>
 <a href="https://www.docker.com/">
   <img src="https://img.shields.io/badge/Docker-latest-blue">
 </a>
 <a href="https://docs.docker.com/compose/">
   <img src="https://img.shields.io/badge/Docker%20Compose-latest-blue">
 </a>
</div>

## 簡介

Gitlab Enterprise Edition Docker Compose 是一個版本控制系統解決方案，他使用 Docker、Docker Compose 建立架構，他使用虛擬化技術，快速建立 Gitlab 與 Gitlab Runner 服務

## 開發

```bash
# 克隆項目
git clone https://gitlab.com/timmyBai/gitlab_enterprise_edition_docker_compose.git
```

## 發佈

```bash
docker compose up --build -d
```

## Gitlab 初始帳密

```textile
# 帳號
root

# 密碼取得
# 進入 docker gitlab bash 環境
docker exec -it [gitlalb container id] /bin/bash

# 進入 gitlab 目錄
cd /etc/gitlab
cat initial_root_password
```

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
